import React, { Component } from 'react';
import Show from './Show';
import {Table} from 'react-bootstrap';
import PropTypes from 'prop-types';

const urlForStation = stationId =>
	`https://mixlr-codetest.herokuapp.com/schedules/${stationId}`;

class Station extends Component {
	constructor(props) {
		super(props);
		this.state = {
			requestFailed: false
		};
		this.handleSort = this.handleSort.bind(this);
	}

	componentDidMount() {
		fetch(urlForStation(this.props.stationId))
			.then(response => {
				if (!response.ok) throw Error('Network request failed');
				return response;
			})
			.then(res => res.json())
			.then(jsonData => {
				this.setState({
					stationData: jsonData
				});
			}, () => {
				this.setState({
					requestFailed: true
				});
			});
	}

	handleSort(event){
		event.preventDefault();
		let newState = {...this.state};
		let sortKey = event.target.dataset.value;
		let sortOrder = (newState.sortOrder)? newState.sortOrder: -1;
		sortOrder = (newState.sortKey === sortKey)? sortOrder*-1 :sortOrder;
		newState.stationData.schedule = this.sortByKey(this.state.stationData.schedule,sortKey,sortOrder);
		newState.sortKey = sortKey;
		newState.sortOrder = sortOrder;
		this.setState(newState);
	}

	sortByKey(array, key, order=1) {
		return array.sort((a, b) => {
			let x = a[key];
			let y = b[key];
			return ((x < y) ? -1 : ((x > y) ? 1 : 0)) * order;
		});
	}

	render() {
		if (this.state.requestFailed) return <p>Failed!</p>;
		if (!this.state.stationData) return <p>Loading...</p>;
		return (
			<div>
				<h2>{this.state.stationData.name}</h2>
				<Table striped bordered condensed hover>
					<thead>
						<tr>
							<th>Image</th>
							<th data-value="title" onClick={this.handleSort}>Title</th>
							<th data-value="host" onClick={this.handleSort}>Host</th>
							<th data-value="kind" onClick={this.handleSort}>Kind</th>
							<th data-value="starts_at" onClick={this.handleSort}>Starts at</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						{this.state.stationData.schedule.map((showData,index) =>{
							return (<Show key={index} showData={showData} />);
						})}
					</tbody>
				</Table>
			</div>
		);
	}
}

export default Station;


Station.propTypes = {
	stationId:PropTypes.string
};