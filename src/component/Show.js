import React, { Component } from 'react';
import {Button, Image} from 'react-bootstrap';
import PropTypes from 'prop-types';

class Show extends Component {
	constructor(props) {
		super(props);
		this.handleStart = this.handleStart.bind(this);
		this.handleStop = this.handleStop.bind(this);
	}

	handleStart(event){
		event.preventDefault();
		console.log("start");
	}

	handleStop(event){
		event.preventDefault();
		console.log("stop");
	}

	render() {
		let startDate = new Date(this.props.showData.starts_at);
		return (
			<tr>
				<td><Image thumbnail  width="50" height="50" src={this.props.showData.image_url} /></td>
				<td>{this.props.showData.title}</td>
				<td>{this.props.showData.host}</td>
				<td>{this.props.showData.kind}</td>
				<td>{startDate.toLocaleDateString()+' at '+startDate.toLocaleTimeString()}</td>                
				<td>
					{this.props.showData.kind === 'live' && <Button onClick={this.handleStart} bsStyle="success">Start</Button>} 
					{this.props.showData.status === 'on_air' && <Button onClick={this.handleStop} bsStyle="danger">Stop</Button>} 
				</td>
			</tr>
		);
	}
}
export default Show;

Show.propTypes = {
	showData:PropTypes.object
};