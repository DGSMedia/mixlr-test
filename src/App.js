import React, { Component } from 'react';
import './css/App.css';

import Station from './component/Station.js';

class App extends Component {
	render() {
		return (
			<div className="App">
				<Station stationId="10"></Station> {/*more station can be added  with a select option or with a map function that render all of them from an arry.*/}
			</div>
		);
	}
}

export default App;
