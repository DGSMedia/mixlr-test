
## Getting Started

clone this repository on your local environment by using:
```
git clone https://Degrand91@bitbucket.org/Degrand91/mixlr-test.git
```

### Installing

For run this application you have to install some dipendency first so run :

```
npm install
```

## Running the app

to run the app simply execute on your terminal the following command
```
npm start
```

to see the final result just visit

```
http://localhost:3000
```

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details